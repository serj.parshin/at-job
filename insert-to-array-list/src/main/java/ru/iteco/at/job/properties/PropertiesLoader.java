package ru.iteco.at.job.properties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {

    private static PropertiesLoader ourInstance = new PropertiesLoader();
    private final Properties properties = new Properties();
    private final ClassLoader cl = PropertiesLoader.class.getClassLoader();

    public static PropertiesLoader getInstance() {
        return ourInstance;
    }

    private PropertiesLoader() {
        try (InputStream stream = cl.getResourceAsStream("application.properties")){
            properties.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Properties getProperties(){
        return properties;
    }
}
